# SwiftElapsedTime

[![CI Status](https://img.shields.io/travis/Andrew Benson/SwiftElapsedTime.svg?style=flat)](https://travis-ci.org/Andrew Benson/SwiftElapsedTime)
[![Version](https://img.shields.io/cocoapods/v/SwiftElapsedTime.svg?style=flat)](https://cocoapods.org/pods/SwiftElapsedTime)
[![License](https://img.shields.io/cocoapods/l/SwiftElapsedTime.svg?style=flat)](https://cocoapods.org/pods/SwiftElapsedTime)
[![Platform](https://img.shields.io/cocoapods/p/SwiftElapsedTime.svg?style=flat)](https://cocoapods.org/pods/SwiftElapsedTime)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

SwiftElapsedTime is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'SwiftElapsedTime'
```

## Author

Andrew Benson, drewbenson@netjack.com

## License

SwiftElapsedTime is available under the MIT license. See the LICENSE file for more info.
