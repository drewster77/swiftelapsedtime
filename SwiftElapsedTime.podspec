#
# Be sure to run `pod lib lint SwiftElapsedTime.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'SwiftElapsedTime'
  s.version          = '0.1.0'
  s.summary          = 'A simple tool for measuring and displaying elapsed time in Swift.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
SwiftElapsedTime is a simple struct to measure and display elapsed time in your Swift code.
                       DESC

  s.homepage         = 'https://github.com/Andrew Benson/SwiftElapsedTime'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Andrew Benson' => 'db@nuclearcyborg.com' }
  s.source           = { :git => 'git@bitbucket.org:drewster77/swiftelapsedtime.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '11.0'

  s.source_files = 'SwiftElapsedTime/Classes/**/*'
  
  # s.resource_bundles = {
  #   'SwiftElapsedTime' => ['SwiftElapsedTime/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
